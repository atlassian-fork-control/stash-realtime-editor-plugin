package com.atlassian.stash.plugin.editor;

import com.atlassian.stash.repository.Repository;

public interface CollaborationService {

    boolean isEnabledForRepository(Repository repository);

    void setEnabledForRepository(Repository repository, final boolean enabled);
}
